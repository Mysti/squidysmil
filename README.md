# Yousmil

## Script python

Les scripts python servent à créer le smil. Ils sont appelés par l'API "smil".

Pour utiliser le script il faut au minimum :
* Une video
* Le power point utilisé dans la vidéo

## Utilisation python

```
usage: main.py [-h] [-o OUT] [-i INFO] [-of OUTFILE] [-s SHOW] inVideo inpptx

positional arguments:
  inVideo               the video file
  inpptx                the pptx file

optional arguments:
  -h, --help            show this help message and exit
  -o OUT, --out OUT     print result in the console
  -i INFO, --info INFO  print information
  -of OUTFILE, --outFile OUTFILE
                        Output location of generated files
  -s SHOW, --show SHOW  Show progress`
```

`python main.py [MaVideo] [MonPPTX]`

## Route de l'API data

### API Data
Réalisation de l'API Data
Pour le lancer il vous faut nodeJs. Une fois installé lancer les commandes suivantes dans la console
```
npm i
npm run start:dev
```
Vous pouvez maintenant tester les routes sur : `localhost:3000`
#### Les entités
```js
User:
{
    idUser: number,
    lastName: string,
    firstName: string,
    email: string,
    userName: string,
    password: string,
    deleted: boolean
}

Diapo:
{
    idDiapo: number,
    path: string,
    title: string,
    description: string,
    click: number,
    creation_date: Date,
    number_total: number,
    time: number,
    deleted: boolean
}

Likes_User:
{
    type: string;
}

Commentary_User:
{
    date_cration: Date,
    commentary: string,
    deleted: boolean
}

```
#### ROUTES que vous pouvez tester sur POSTMAN

##### User
| Méthode |    Routes   | Paramètres | Corps | Retour |              Commentaire             |
|---------|:-----------:|:----------:|:-----:|--------|:------------------------------------:|
|   PUT   |    /user    |            |  User |  User  |         créer un utilisateur         |
|   GET   |  /user/list |            |       | User[] | retourne l'ensemble des utilisateurs |
|   PUT   | /user/login |            |  User | User   |        connecte un utilisateur       |
|   GET   |  /user/:id  |     id     |       |  User  |    retourne l'utilisateur demandé    |
|  DELETE |  /user/:id  |     id     |       |  User  |    supprime l'utilisateur demandé    |

##### Diapo
| Méthode |    Routes    | Paramètres |  Corps | Retour   |           Commentaire           |
|---------|:------------:|:----------:|:------:|----------|:-------------------------------:|
|   PUT   |    /diapo    |            |  Diapo |   Diapo  |         créer une diapo         |
|   GET   |  /diapo/:id  |     id     |        |   Diapo  |    retourne la diapo demandé    |
|  DELETE |  /diapo/:id  |     id     |        |   Diapo  |    supprime la diapo demandé    |

##### Likes_User
| Méthode |    Routes    | Paramètres |  Corps  | Retour   |           Commentaire           |
|---------|:------------:|:----------:|:-------:|----------|:-------------------------------:|
|   PUT   |  /likesUser  |            |LikesUser| LikesUser|          créer un like          |
|   GET   |/likesUser/:id|     id     |         | LikesUser|     retourne le like demandé    |
|  DELETE |/likesUser/:id|     id     |         | LikesUser|     supprime la like demandé    |

##### Commentary_User
| Méthode |       Routes      | Paramètres |     Corps    |    Retour    |           Commentaire           |
|---------|:-----------------:|:----------:|:------------:|--------------|:-------------------------------:|
|   PUT   |  /commentaryUser  |            |CommentaryUser|CommentaryUser|       créer un commentaire      |
|   GET   |/commentaryUser/:id|     id     |              |CommentaryUser| retourne le commentaire demandé |
|  DELETE |/commentaryUser/:id|     id     |              |CommentaryUser| supprime la commentaire demandé |

## Installation et lancement du FRONT

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

```
npm install -g @angular/cli
npm i 
ng serve
```