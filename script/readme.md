# Script python

Les scripts python servent à créer le smil. Ils sont appelés par l'API "smil".

Pour utiliser le script il faut au minimum :
* Une video
* Le power point utilisé dans la vidéo

# Utilisation

```
usage: main.py [-h] [-o OUT] [-i INFO] [-of OUTFILE] [-s SHOW] inVideo inpptx

positional arguments:
  inVideo               the video file
  inpptx                the pptx file

optional arguments:
  -h, --help            show this help message and exit
  -o OUT, --out OUT     print result in the console
  -i INFO, --info INFO  print information
  -of OUTFILE, --outFile OUTFILE
                        Output location of generated files
  -s SHOW, --show SHOW  Show progress`
```

`python main.py [MaVideo] [MonPPTX]`