# coding: utf-8

from __future__ import print_function

# Standard PySceneDetect imports:
from scenedetect.video_manager import VideoManager
from scenedetect.scene_manager import SceneManager
# For caching detection metrics and saving/loading to a stats file
from scenedetect.stats_manager import StatsManager

# For content-aware scene detection:
from scenedetect.detectors.content_detector import ContentDetector

from pptx import Presentation
import os
import argparse
import csv
import json

class Part:
    def __init__(self, c_diapo, c_start, c_end, c_title):
        self.scene = c_diapo
        self.start = c_start
        self.end = c_end
        self.title = c_title

def find_titles(filename):
    prs = Presentation(filename)
    titles = []
    for slide in prs.slides:
        title = slide.shapes.title.text
        titles.append(title)
    return titles

def find_scenes(video_path, pptx_path, threshold=2, min_percent=40, information=False, out=True, outFile="./", show = False):
    # type: (str) -> List[Tuple[FrameTimecode, FrameTimecode]]
    video_manager = VideoManager([video_path])
    stats_manager = StatsManager()
    # Construct our SceneManager and pass it our StatsManager.
    scene_manager = SceneManager(stats_manager)

    # Add ContentDetector algorithm (each detector's constructor
    # takes detector options, e.g. threshold).
    scene_manager.add_detector(ContentDetector(threshold))

    # We save our stats file to {VIDEO_PATH}.stats.csv.
    video_name = os.path.splitext(os.path.basename(video_path))[0]
    stats_file_path = os.path.join(outFile, '%s.stats.csv' % video_name)
    data_file_path = os.path.join(outFile, '%s.data.json' % video_name)

    scene_list = []

    try:
        # If stats file exists, load it.
        if os.path.exists(stats_file_path):
            # Read stats from CSV file opened in read mode:
            with open(stats_file_path, 'r') as stats_file:
                stats_manager.load_from_csv(stats_file)

        # Set downscale factor to improve processing speed.
        video_manager.set_downscale_factor()

        # Start video_manager.
        video_manager.start()

        # Perform scene detection on video_manager.
        scene_manager.detect_scenes(frame_source=video_manager, show_progress=show)

        # Obtain list of detected scenes.
        scene_list = scene_manager.get_scene_list()
        # Each scene is a tuple of (start, end) FrameTimecodes.
        parts = []
        titles = find_titles(pptx_path)
        scenes = []

        if(information):
            print('List des diapos :')

        for i, scene in enumerate(scene_list):
            scenes.append(scene[0].get_timecode())
            p = Part(i + 1, scene[0].get_timecode(), scene[1].get_timecode(), titles[i])
            parts.append(p)

            if(information):
                print(
                    'diapo %2d: Start %s , End %s, Title => %s' % (
                        i + 1,
                        scene[0].get_timecode(),
                        scene[1].get_timecode(), titles[i]))



        if(not out):
            print(json.dumps(parts, default=lambda o: o.__dict__, sort_keys=True, indent=4))
        else:
            # We only write to the stats file if a save is required:
            if stats_manager.is_save_required():
                base_timecode = video_manager.get_base_timecode()
                with open(stats_file_path, 'w') as stats_file:
                    stats_manager.save_to_csv(stats_file, base_timecode)
            """
            data = {}
            with open(stats_file_path) as csvFile:
                csvReader = csv.DictReader(csvFile)
                for rows in csvReader:
                    id = rows['Timecode']
                    data[id] = rows
            """
            with open(data_file_path, 'w') as jsonFile:
                jsonFile.write(json.dumps(parts, default=lambda o: o.__dict__, sort_keys=True, indent=4))

    finally:
        video_manager.release()

    return scene_list

parser = argparse.ArgumentParser()
parser.add_argument("inVideo", type=str, help="the video file")
parser.add_argument("inpptx", type=str, help="the pptx file")
parser.add_argument("-o", "--out", type=bool, help="printr result in the console")
parser.add_argument("-i", "--info", type=bool, help="print information")
parser.add_argument("-of", "--outFile", type=str, help="Output location of generated files")
parser.add_argument("-s", "--show", type=str, help="Show progress")
args = parser.parse_args()

if __name__ == "__main__":
    find_scenes(
        args.inVideo, 
        args.inpptx, 
        information = args.info,
        out = args.out,
        show=args.show)
