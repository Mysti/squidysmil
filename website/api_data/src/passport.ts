const Config = require("./config/config");

import * as passport from 'passport';
import * as jwt from 'jsonwebtoken'
import axios from 'axios';


import { Application,Request, Response } from "express";
import { ExtractJwt, StrategyOptions, Strategy as JwtStrategy } from 'passport-jwt';
import { TokenExpiredError, JsonWebTokenError } from "jsonwebtoken";
import { getRepository } from 'typeorm';
import { UserEntity } from './entity/user.entity';

export default class Passport{

    public static configure(app: Application) {
        app.use(passport.initialize());

        //jwt option
        const jwtOptions: StrategyOptions = {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: Config.get('jwt').secret,
            ignoreExpiration: false
        };

        //jwt strategy
        passport.use('jwt', new JwtStrategy(jwtOptions, (jwt_payload, done) => {

            getRepository(UserEntity)
            .findOne(jwt_payload.id)
            .then(user => {
            if(user){
                delete user.password;
                return done(null, user);
            }
            return done(null, false);
        })
        .catch(error => { 
            return done(null, false);
        })

        }));
    }

    public static checkJwtToken(req: Request, res: Response, next: any) {

        return passport.authenticate('jwt', { session: false, failWithError:true, }, function(err, user, jwtPayload) {
            if (jwtPayload && jwtPayload instanceof Error) {
                let err = jwtPayload;
        
                let status = 401;
                let code = 40101;
                let message = 'No auth token'
        
                if (err instanceof TokenExpiredError) {
                    code = 40103;
                    message = 'Token expired';
                } else if (err instanceof JsonWebTokenError) {
                    code = 40102;
                    message = 'Token invalid';
                } else if (err instanceof Error){
                    code = 40101;
                    message = 'No auth token';
                }

                return res.status(401).json({status, code, message});
            }

            
            if(!req.headers.authorization)
                return res.status(503).json({title: "unknown error"});

            let token = req.headers.authorization.replace("Bearer ","");
            let decoded: any = jwt.decode(token, {complete: true});

            let id: any = decoded.payload.id;
            
            getRepository(UserEntity)
            .findOne(id)
            .then(resUser => {
                if(!resUser){
                    let status = 401;
                    let code = 40101;
                    let message = 'No auth token'
                    return res.status(401).json({status, code, message});
                }
                
                res.locals.user = resUser;
                res.locals.payload = jwtPayload;
                next();
            })
            .catch(err => {
                if(err){
                    return res.status(500).json(err);
                }

                let status = 500;
                let code = 500;
                let message = "unknow error"
                return res.status(status).json({status, code, message});
            })
        })(req,res)
    }

    public static signToken(payload: any) {
        return jwt.sign(payload, Config.get('jwt').secret, { expiresIn: Config.get('jwt').expires_in });
    }
}