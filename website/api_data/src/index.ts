/**
 * @author DE BRUYNE Alexis - LECELLIER Laetitia
*/

const Config = require("./config/config");
import { Server } from "./server";

const host = Config.get("host");
const port = Config.get("port");

const server = new Server(host,port, Config.get("db"));
server.start();