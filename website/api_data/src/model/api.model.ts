export class ApiSuccess {
    private data: any|any[]
    private links?: Partial<ApiSuccessLinks>;

    constructor(data?: any|any[], links?: Partial<ApiSuccessLinks>) {
        this.data = data;
        links && (this.links = links);
    }
}

export class ApiError {
    private errors: ApiErrorItem[]=[];

    constructor(errors: ApiErrorItem|ApiErrorItem[]) {
        if ( !Array.isArray(errors)) {
            errors = [ errors];
        }

        errors.forEach(error => this.addError(error))
    }

    public addError(error: ApiErrorItem) {
        if (process.env.NODE_ENV == 'production') {
            delete error.detail;
            delete error.more;
        }

        this.errors.push(error);
    }
}

/**
 * status:  the HTTP status code applicable to this problem, expressed as a string value
 * code:    an application-specific error code, expressed as a string value.
 * title:   a short, human-readable summary of the problem 
 * detail:  a human-readable explanation specific to this occurrence of the problem
 * more:    stack trace
 * 
 * @see https://jsonapi.org/format/1.1/#error-objects
 */
declare type ApiErrorItem= {
    status?: number;
    code?: number|string;
    title: string;
    detail?: string;
    more?: string
}

/**
 * @see https://jsonapi.org/format/1.1/#document-links
 */
declare type ApiSuccessLinks = {
    self: string,   // current page url, http://example.com/article
    next: string,   // next page url, http://example.com/article?offset=2,
    last?: string,  // last page url, http://example.com/article?offset=10
    total?: number, // page number 
}