import * as express from "express";
import * as bodyParser from "body-parser";
import * as glob from "glob";
import * as path from "path";
import * as fs from 'fs';
import * as cors from "cors";
import * as schedule from "node-schedule";
import Passport from "./passport";
import { Connection, createConnection } from "typeorm";
import { FileMiddleware } from "./middleware/file.middleware";

export class Server{

    private host: string;
    private port: number;
    private app: any;

    constructor(host: string, port: number, dataBaseConfig: any){
        this.host = host;
        this.port = port;
        this.app = express();

        this.app.use(bodyParser.json({
            limit: '50mb',
            type: '*/json'
        }));

        this.app.use(cors())

        let dirtemp = path.join(__dirname, FileMiddleware.DIR_TEMP)
        if (!fs.existsSync(dirtemp)){
            fs.mkdirSync(dirtemp, {recursive: true});
        }

        let dirpic = path.join(__dirname, FileMiddleware.DIR_PICTURE)
        if (!fs.existsSync(dirpic)){
            fs.mkdirSync(dirpic, {recursive: true});
        }


        // Make Folder Publicly Available
        this.app.use('/public/pictures', express.static('public/pictures'));
        this.app.use('/public/temps', express.static('public/temps'));

        this.setRoute();
        this.setDataBase(dataBaseConfig);
        Passport.configure(this.app);

    }

    private setRoute(){
        glob.sync(path.join(__dirname,'./routes/*.routes.*s')).forEach(route => {
            console.log(`loading ${route}...`)
            require(path.resolve(route))(this.app);
        });
        console.log(`routes loaded`)
    }

    private setDataBase(dataBaseConfig: any){
        createConnection(dataBaseConfig).then((connection: Connection) => {
            this.app.use(bodyParser.json({
                limit: '50mb'
            }));
        });
    }

    start(){
        this.app.listen(this.port, () => {
            console.log(`Server started on port ${this.host}:${this.port}.`)
        });

    }

}

let deleteFileschedule = schedule.scheduleJob('*/30 * * * ', function(){
    glob(`${FileMiddleware.DIR_TEMP}`, (err, files) => {
        if(err){
            console.error(err)
        }

        files.forEach(file => {
            let fileStat = fs.statSync(file);

            let hourMillisecond = 3600000;
            let now = new Date();;
            let result = Math.abs(now.getTime() - fileStat.birthtime.getTime()) / (hourMillisecond); 

            if(result >= 1){
                try{
                    fs.unlinkSync(file)
                } catch(err){
                    console.error(err);
                }

            }
        });
    })
});
