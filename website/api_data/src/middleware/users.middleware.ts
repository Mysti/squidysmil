import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { UserEntity } from "../entity/user.entity";
import * as bcrypt from "bcrypt";
import Passport from "../passport";
import { ApiError, ApiSuccess } from "../model/api.model";

export class UserMiddleware{

    /**
     * Sauvegarde un utilisateur
     * La requête doit contenir l'utilisateur à sauvegarder
     */
    public static put(req: Request, res: Response, next: any){
        const user: UserEntity = Object.assign(new UserEntity(), req.body);

        if(user.password){
            user.password = bcrypt.hashSync(user.password, 10);
        }

        getRepository(UserEntity).save(user)
        .then(user => {
            delete user.password;
            return res.status(200).json(new ApiSuccess(user));
        })
        .catch((error: any) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

    /**
     * Récupère un utilisateur
     * La requête doit contenir l'id de l'utilisateur
     */
    public static get(req: Request, res: Response, next: any){
        const id = req.params.id;

        getRepository(UserEntity)
        .findOne(id)
        .then(user => {
            if(user){
                delete user.password;
                return res.status(200).json(new ApiSuccess(user));
            }
            return res.status(404).json(new ApiError({
                title: "Not found",
                detail: "User not found",
            }));
        })
        .catch((error: any) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

    /**
     * Récupère l'utilisateur de le demande
     * La requête doit contenir le token dans le header
     */
    public static getme(req: Request, res: Response, next: any){
        const user = res.locals.user;
        delete user.password;
        return res.status(200).json(new ApiSuccess(user));
    }

    /**
     * Récupère les utilisateurs
     */
    public static getList(req: Request, res: Response, next: any){

        getRepository(UserEntity)
        .find()
        .then(users => {
            if(users){
                return res.status(200).json(new ApiSuccess(users));
            }
            return res.status(404).json(new ApiError({
                title: "Not found",
                detail: "Users not found",
            }));
        })
        .catch((error: any) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

    /**
     * Login
    */
    public static login(req: Request, res: Response, next: any){
        let username = req.body.username;
        let password = req.body.password;

        getRepository(UserEntity)
        .findOne({username})
        .then(user => {
            if(user){
                if(bcrypt.compareSync(password, user.password)){
                    delete user.password;
                    
                    let token = Passport.signToken({
                        id: user.idUser
                    }); 
    
                    res.set({
                        "Access-Control-Expose-Headers": "Authorization",
                        "Authorization": `Bearer ${token}`
                    });

                    return res.status(200).json(new ApiSuccess(user));
                }

                return res.status(403).json(new ApiError({
                    title: "Access denied",
                    detail: "Bad pseudo or password",
                }));
            }
            return res.status(403).json(new ApiError({
                title: "Access denied",
                detail: "Bad pseudo or password",
            }));
        })
        .catch(error => {
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

    /**
     * Supprime un utilisateur
     * La requête doit contenir l'id de l'utilisateur'
     */
    public static delete(req: Request, res: Response, next: any) {
        const id: number = Number.parseInt(req.params.id);
        let user = new UserEntity();
        user.idUser = id;

        getRepository(UserEntity)
        .remove(user)
        .then((responseDelete: any) => {
            return res.status(200).json(new ApiSuccess(responseDelete));
        })
        .catch((error: any) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }
}