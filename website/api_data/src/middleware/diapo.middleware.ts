import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { DiapoEntity } from "../entity/diapo.entity";
import { ApiError, ApiSuccess } from "../model/api.model";

export class DiapoMiddleware{

    /**
     * Sauvegarde une diapo
     * La requête doit contenir une diapo à sauvegarder
     */
    public static put(req: Request, res: Response, next: any){
        const diapo: DiapoEntity = Object.assign(new DiapoEntity(), req.body);

        getRepository(DiapoEntity).save(diapo)
        .then(diapo => {
            return res.status(200).json(new ApiSuccess(diapo));
        })
        .catch((error: Error) => { 
            res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })

    }

    /**
     * Récupère une diapo
     * La requête doit contenir l'id d'une diapo
     */
    public static get(req: Request, res: Response, next: any){
        const id = req.params.id;

        getRepository(DiapoEntity)
        .findOne(id)
        .then(diapo => {
            if(diapo){
                return res.status(200).json(new ApiSuccess(diapo));
            }
            return res.status(404).json(new ApiError({
                title: "Not found",
                detail: "Diapo not found",
            }));
        })
        .catch((error: Error) => { 
            res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

    /**
     * Supprime une diapo
     * La requête doit contenir l'id de la diapo
     */
    public static delete(req: Request, res: Response, next: any) {
        const id: number = Number.parseInt(req.params.id);
        let diapo = new DiapoEntity();
        diapo.idDiapo = id;

        getRepository(DiapoEntity)
        .remove(diapo)
        .then((responseDelete: any) => {
            return res.status(200).json(new ApiSuccess(responseDelete));
        })
        .catch((error: Error) => { 
            res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

}