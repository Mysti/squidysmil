import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Likes_UserEntity } from "../entity/likes_user.entity";
import { ApiSuccess, ApiError } from "../model/api.model";

export class Likes_UserMiddleware{

    /**
     * Sauvegarde un like
     * La requête doit contenir un like à sauvegarder
     */
    public static put(req: Request, res: Response, next: any){
        const likes_User: Likes_UserEntity = Object.assign(new Likes_UserEntity(), req.body);

        getRepository(Likes_UserEntity).save(likes_User)
        .then(likes_User => {
            return res.status(200).json(new ApiSuccess(likes_User));
        })
        .catch((error: Error) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })

    }

    /**
     * Récupère un like
     * La requête doit contenir l'id d'un like
     */
    public static get(req: Request, res: Response, next: any){
        const id = req.params.id;

        const relations = [
            "user",
            "diapo"
        ];

        getRepository(Likes_UserEntity)
        .findOne(id,{relations})
        .then(likes_User => {
            if(likes_User){
                return res.status(200).json(new ApiSuccess(likes_User));
            }
            return res.status(404).json(new ApiError({
                title: "Not found",
                detail: "Likes user not found",
            }));
        })
        .catch((error: Error) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

    /**
     * Supprime un like
     * La requête doit contenir l'id du like
     */
    public static delete(req: Request, res: Response, next: any) {
        const id: number = Number.parseInt(req.params.id);
        let likes_User = new Likes_UserEntity();

        getRepository(Likes_UserEntity)
        .remove(likes_User)
        .then((responseDelete: any) => {
            return res.status(200).json(new ApiSuccess(responseDelete));
        })
        .catch((error: Error) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

}