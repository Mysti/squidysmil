import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Commentary_UserEntity } from "../entity/commentary_user.entity";
import { ApiError, ApiSuccess } from "../model/api.model";

export class Commentary_UserMiddleware{

    /**
     * Sauvegarde un commentaire
     * La requête doit contenir un commentaire à sauvegarder
     */
    public static put(req: Request, res: Response, next: any){
        const commentary_User: Commentary_UserEntity = Object.assign(new Commentary_UserEntity(), req.body);

        getRepository(Commentary_UserEntity).save(commentary_User)
        .then(commentary_User => {
            return res.status(200).json(new ApiSuccess(commentary_User));
        })
        .catch((error: Error) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        });

    }

    /**
     * Récupère un commentaire
     * La requête doit contenir l'id du commentaire
     */
    public static get(req: Request, res: Response, next: any){
        const id = req.params.id;

        const relations = [
            "user",
            "diapo"
        ];

        getRepository(Commentary_UserEntity)
        .findOne(id,{relations})
        .then(commentary_User => {
            if(commentary_User){
                return res.status(200).json(new ApiSuccess(commentary_User));
            }
            return res.status(404).json(new ApiError({
                title: "Not found",
                detail: "Commentary user not found",
            }));
        })
        .catch((error: Error) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

    /**
     * Supprime un commentaire
     * La requête doit contenir l'id du commentaire
     */
    public static delete(req: Request, res: Response, next: any) {
        const id: number = Number.parseInt(req.params.id);
        let commentary_User = new Commentary_UserEntity();

        getRepository(Commentary_UserEntity)
        .remove(commentary_User)
        .then((responseDelete: any) => {
            return res.status(200).json(new ApiSuccess(responseDelete));
        })
        .catch((error: any) => { 
            return res.status(500).json(new ApiError({
                title: error.name,
                detail: error.message,
            }));
        })
    }

}