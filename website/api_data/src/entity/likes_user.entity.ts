import {
    Column,
    Entity,
    ManyToMany,
    PrimaryColumn
} from "typeorm";
import { DiapoEntity } from "./diapo.entity";
import { UserEntity } from "./user.entity";

@Entity("Likes_User")
export class Likes_UserEntity {

    @PrimaryColumn({
        type: "integer"
    })
    idLikes_User: number;

    @Column()
    type: string | undefined;

    @ManyToMany(() => UserEntity, (user: UserEntity) => user.likesUser)
    user: UserEntity[] | undefined;

    @ManyToMany(()=>DiapoEntity,(diapo: DiapoEntity)=>diapo.likesUser)
    diapo: DiapoEntity[] | undefined

}
