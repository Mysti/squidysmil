import {
    Column,
    Entity,
    ManyToMany,
    JoinTable,
    PrimaryColumn,
    OneToMany,
    PrimaryGeneratedColumn,
} from "typeorm";
import { Commentary_UserEntity } from "./commentary_user.entity";
import { DiapoEntity } from "./diapo.entity";
import { Likes_UserEntity } from "./likes_user.entity";

@Entity("Users")
export class UserEntity {

    @PrimaryGeneratedColumn({
        type: "integer"
    })
    idUser: number;

    @Column()
    lastname: string | undefined;

    @Column()
    firstname: string | undefined;

    @Column()
    email: string | undefined;

    @Column()
    username: string | undefined;

    @Column()
    password: string | undefined;

    @Column()
    avatar: string | undefined;

    @Column()
    deleted: boolean | undefined;

    @ManyToMany(()=>DiapoEntity,(diapo: DiapoEntity)=>diapo.User_has_Diapo,{
        cascade:true
    })
    @JoinTable({
        name: "Users_has_Diapo",
        joinColumn: {
            name: "Users_idUser",
            referencedColumnName: "idUser"
        },
        inverseJoinColumn:{
            name: "Diapo_idDiapo",
            referencedColumnName: "idDiapo"
        }
    })
    User_has_Diapo: DiapoEntity[] | undefined

    @OneToMany(()=>Likes_UserEntity,(likeUser: Likes_UserEntity)=>likeUser.user,{
        cascade:true
    })
    likesUser: Likes_UserEntity[] | undefined

    @OneToMany(()=>Commentary_UserEntity,(commentaryUser: Commentary_UserEntity)=>commentaryUser.user,{
        cascade:true
    })
    commentaryUser: Commentary_UserEntity[] | undefined

}
