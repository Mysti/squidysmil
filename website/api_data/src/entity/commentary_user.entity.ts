import {
    Column,
    Entity,
    ManyToMany,
    PrimaryColumn
} from "typeorm";
import { DiapoEntity } from "./diapo.entity";
import { UserEntity } from "./user.entity";

@Entity("Commentary_User")
export class Commentary_UserEntity {
    @PrimaryColumn({
        type: "integer"
    })
    idCommentary_User: number;

    @Column()
    date_cration: Date | undefined;

    @Column()
    commentary: String | undefined;

    @Column()
    deleted: boolean | undefined;

    @ManyToMany(() => UserEntity, (user: UserEntity) => user.commentaryUser)
    user: UserEntity[] | undefined;

    @ManyToMany(()=>DiapoEntity,(diapo: DiapoEntity)=>diapo.commentaryUser)
    diapo: DiapoEntity[] | undefined

}
