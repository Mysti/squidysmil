import {
    Column,
    Entity,
    ManyToMany,
    OneToMany,
    PrimaryColumn,
    PrimaryGeneratedColumn,
} from "typeorm";
import { Commentary_UserEntity } from "./commentary_user.entity";
import { Likes_UserEntity } from "./likes_user.entity";
import { UserEntity } from "./user.entity";

@Entity("Diapo")
export class DiapoEntity {

    @PrimaryGeneratedColumn({
        type: "integer"
    })
    idDiapo: number;

    @Column()
    path: string | undefined;

    @Column()
    title: string | undefined;

    @Column()
    description: string | undefined;

    @Column()
    click: number | undefined;

    @Column()
    creation_Date: Date | undefined;

    @Column()
    number_Total: number | undefined;

    @Column()
    time: number | undefined;

    @Column()
    deleted: boolean | undefined;

    @ManyToMany(()=>UserEntity,(user: UserEntity)=>user.User_has_Diapo)
    User_has_Diapo: UserEntity[] | undefined

    @OneToMany(()=>Likes_UserEntity,(likeUser: Likes_UserEntity)=>likeUser.diapo)
    likesUser: Likes_UserEntity[] | undefined

    @OneToMany(()=>Commentary_UserEntity,(commentaryUser: Commentary_UserEntity)=>commentaryUser.diapo)
    commentaryUser: Commentary_UserEntity[] | undefined


}
