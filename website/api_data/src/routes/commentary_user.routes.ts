import { Application } from "express-serve-static-core";
import { Commentary_UserMiddleware } from "../middleware/commentary_user.middleware";

module.exports = function(app: Application){
    app.route("/commentaryUser")
    .put(
        Commentary_UserMiddleware.put
    )

    app.route("/commentaryUser/:id")
    .get(
        Commentary_UserMiddleware.get
    )
    .delete(
        Commentary_UserMiddleware.delete
    );

}