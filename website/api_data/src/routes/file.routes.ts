import { Application } from "express-serve-static-core";
import { FileMiddleware } from "../middleware/file.middleware";
import { UserMiddleware } from "../middleware/users.middleware";
import Passport from "../passport";


module.exports = function(app: Application){
    app.route("/file/avatar")
    .post(
        Passport.checkJwtToken,
        FileMiddleware.upload_image(true),
        FileMiddleware.sendFileLink,
    )
    .delete(
        Passport.checkJwtToken,
        FileMiddleware.deleteFile
    )
}