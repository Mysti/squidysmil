import { Application } from "express-serve-static-core";
import { Likes_UserMiddleware } from "../middleware/Likes_User.middleware";

module.exports = function(app: Application){
    app.route("/likesUser")
    .put(
        Likes_UserMiddleware.put
    )

    app.route("/likesUser/:id")
    .get(
        Likes_UserMiddleware.get
    )
    .delete(
        Likes_UserMiddleware.delete
    );

}