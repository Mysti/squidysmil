import { Application } from "express-serve-static-core";
import { DiapoMiddleware } from "../middleware/diapo.middleware";

module.exports = function(app: Application){
    app.route("/diapo")
    .put(
        DiapoMiddleware.put
    )

    app.route("/diapo/:id")
    .get(
        DiapoMiddleware.get
    )
    .delete(
        DiapoMiddleware.delete
    );

}