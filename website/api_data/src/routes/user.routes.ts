import { Application } from "express-serve-static-core";
import { PassThrough } from "stream";
import { UserMiddleware } from "../middleware/users.middleware";
import Passport from "../passport";

module.exports = function(app: Application){
    app.route("/user")
    .put(
        UserMiddleware.put
    );

    app.route("/user/list")
    .get(
        UserMiddleware.getList
    )

    app.route("/user/login")    
    .post(
        UserMiddleware.login
    )

    app.route("/user/me")
    .get(
        Passport.checkJwtToken,
        UserMiddleware.getme
    )

    app.route("/user/:id")
    .get(
        UserMiddleware.get
    )
    .delete(
        UserMiddleware.delete
    );
    
}