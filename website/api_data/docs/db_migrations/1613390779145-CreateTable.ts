import {
    MigrationInterface,
    QueryRunner,
    Table,
    TableForeignKey
} from "typeorm";

export class CreateTable1613390779145 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        //Users table
        await queryRunner.createTable(new Table({
            name: "Users",
            columns: [
                {
                    name: "idUser",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true
                },
                {
                    name: "avatar",
                    type: "TEXT",
                    isNullable: true
                },
                {
                    name: "lastname",
                    type: "VARCHAR(100)"
                },
                {
                    name: "firstname",
                    type: "VARCHAR(100)"
                },
                {
                    name: "email",
                    type: "VARCHAR(100)"
                },
                {
                    name: "username",
                    type: "VARCHAR(45)"
                },
                {
                    name: "password",
                    type: "TEXT"
                },
                {
                    name: "deleted",
                    type: "boolean"
                }
            ]
        }));

        //Diapo table
        await queryRunner.createTable(new Table({
            name: "Diapo",
            columns: [
                {
                    name: "idDiapo",
                    type: "int",
                    isPrimary: true,
                    isGenerated: true
                },
                {
                    name: "path",
                    type: "TEXT"
                },
                {
                    name: "title",
                    type: "VARCHAR(45)"
                },
                {
                    name: "description",
                    type: "VARCHAR(45)"
                },
                {
                    name: "click",
                    type: "INT"
                },
                {
                    name: "creation_Date",
                    type: "timestamp"
                },
                {
                    name: "number_Total",
                    type: "INT"
                },
                {
                    name: "time",
                    type: "INT"
                },
                {
                    name: "deleted",
                    type: "boolean"
                }
            ]
        }));
  
  
        //Users_has_Diapo table
        await queryRunner.createTable(new Table({
            name: "Users_has_Diapo",
            columns: [
                {
                    name: "Users_idUser",
                    type: "int",
                },
                {
                    name: "Diapo_idDiapo",
                    type: "int",
                }
            ]
        }));
        await queryRunner.createForeignKey("Users_has_Diapo", new TableForeignKey({
            columnNames: ["Users_idUser"],
            referencedColumnNames: ["idUser"],
            referencedTableName: "Users"
        }));
        await queryRunner.createForeignKey("Users_has_Diapo", new TableForeignKey({
            columnNames: ["Diapo_idDiapo"],
            referencedColumnNames: ["idDiapo"],
            referencedTableName: "Diapo"
        }));
                         
        //Likes_Users table
        await queryRunner.createTable(new Table({
            name: "Likes_Users",
            columns: [
                {
                    name: "Users_idUser",
                    type: "int",
                },
                {
                    name: "Diapo_idDiapo",
                    type: "int",
                },
                {
                    name: "type",
                    type: "VARCHAR(6)",
                }
            ]
        }));
        await queryRunner.createForeignKey("Likes_Users", new TableForeignKey({
            columnNames: ["Users_idUser"],
            referencedColumnNames: ["idUser"],
            referencedTableName: "Users"
        }));
        await queryRunner.createForeignKey("Likes_Users", new TableForeignKey({
            columnNames: ["Diapo_idDiapo"],
            referencedColumnNames: ["idDiapo"],
            referencedTableName: "Diapo"
        }));

        //Commentary_Users table
        await queryRunner.createTable(new Table({
            name: "Commentary_Users",
            columns: [
                {
                    name: "Users_idUser",
                    type: "int",
                },
                {
                    name: "Diapo_idDiapo",
                    type: "int",
                },
                {
                    name: "date_Creation",
                    type: "timestamp",
                },
                {
                    name: "commentary",
                    type: "TEXT",
                },
                 {
                    name: "deleted",
                    type: "boolean",
                }
            ]
        }));
        await queryRunner.createForeignKey("Commentary_Users", new TableForeignKey({
            columnNames: ["Users_idUser"],
            referencedColumnNames: ["idUser"],
            referencedTableName: "Users"
        }));
        await queryRunner.createForeignKey("Commentary_Users", new TableForeignKey({
            columnNames: ["Diapo_idDiapo"],
            referencedColumnNames: ["idDiapo"],
            referencedTableName: "Diapo"
        }));

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        //DELETE FK Users_has_Diapo
        let table  = await queryRunner.getTable("Users_has_Diapo");
        let foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("Users_idUser") !== -1);
        await queryRunner.dropForeignKey("Users_has_Diapo", foreignKey);

        table = await queryRunner.getTable("Users_has_Diapo");
        foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("Diapo_idDiapo") !== -1);
        await queryRunner.dropForeignKey("Users_has_Diapo", foreignKey);

        //DELETE FK Likes_Users
        table = await queryRunner.getTable("Likes_Users");
        foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("Users_idUser") !== -1);
        await queryRunner.dropForeignKey("Likes_Users", foreignKey);

        table = await queryRunner.getTable("Likes_Users");
        foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("Diapo_idDiapo") !== -1);
        await queryRunner.dropForeignKey("Likes_Users", foreignKey);


        //DELETE FK Commentary_Users
        table = await queryRunner.getTable("Commentary_Users");
        foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("Users_idUser") !== -1);
        await queryRunner.dropForeignKey("Commentary_Users", foreignKey);

        table = await queryRunner.getTable("Commentary_Users");
        foreignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("Diapo_idDiapo") !== -1);
        await queryRunner.dropForeignKey("Commentary_Users", foreignKey);


        await queryRunner.dropTable("Users")
        await queryRunner.dropTable("Diapo")

        await queryRunner.dropTable("Users_has_Diapo")
        await queryRunner.dropTable("Likes_Users")
        await queryRunner.dropTable("Commentary_Users")
    }

}
