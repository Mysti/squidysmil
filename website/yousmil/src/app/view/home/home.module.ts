import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { SharedModule } from 'src/app/@shared/shared.module';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { ReactiveFormsModule } from '@angular/forms';
import { AvatarModule } from 'ngx-avatar';
import { MyVideoComponent } from './my-video/my-video.component';
import { SmileService } from 'src/app/services/smil.service';

@NgModule({
  declarations: [
    HomeComponent,
    ProfileComponent,
    MyVideoComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    FlashMessagesModule,
    ReactiveFormsModule,
    AvatarModule
  ],
  providers: [
    SmileService
  ]
})
export class HomeModule { }
