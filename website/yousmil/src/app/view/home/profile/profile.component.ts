import { Component, OnDestroy, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Subscription } from 'rxjs';
import { ProfileForm, ProfileFormProvider } from 'src/app/forms/profile.form';
import { User } from 'src/app/model/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [
    ProfileFormProvider
  ]
})
export class ProfileComponent implements OnInit, OnDestroy {

  public user: User;
  private userSubscription: Subscription;

  public loading = false;
  public hide: boolean = true;

  constructor(
    public form: ProfileForm,
    private AuthService: AuthService,
    private UserService: UserService,
    private FlashService: FlashMessagesService ) { }

  ngOnInit(): void {
    this.userSubscription = this.AuthService.userSubject.subscribe(user =>{
      this.user = user
      this.form.fillValues(user);
    });
    this.AuthService.emitUser();
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  submitForm(): void {
    this.UserService.save(this.form.getValues()).subscribe(
      val => {
      this.loading = false;
      this.AuthService.setUser(Object.assign(new User(), val));
      this.FlashService.show(`User update Successfully`, { cssClass: 'alert-success'})

    },
    err => {
      console.log(err);
    });
  }

  OnFileSelected(event){
    const file = Array.from(event.target.files)[0];
    if(file){
      this.UserService.upload_avatar(file).subscribe((res: any) => {
        console.log(res);
        this.user.avatar = res.path;
        this.form.setAvatar(res.path);
      },
      err => console.log(err))
    }
    console.log(file)
  }

  onDeleteAvatar(){
    if(this.user.avatar)
      this.UserService.delete_avatar(this.user.avatar).subscribe(res =>{
        this.user.avatar = null;
        this.form.setAvatar(null);
      });
  }
}
