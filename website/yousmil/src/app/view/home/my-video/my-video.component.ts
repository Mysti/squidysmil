import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SmileService } from 'src/app/services/smil.service';

@Component({
  selector: 'app-my-video',
  templateUrl: './my-video.component.html',
  styleUrls: ['./my-video.component.scss']
})
export class MyVideoComponent implements OnInit {

  public video: {
    title: string,
    url: string,
  };

  public pptx: {
    title: string,
    url: string,
  }

  public PercentVideo: number;
  public PercentPPTX: number;

  public LoadingVideo: boolean;
  public LoadingPPTX: boolean;
  public LoadingCreate: boolean;

  public JsonValue;

  constructor(private SmilService: SmileService) { 
    this.video = {
      title: undefined,
      url: undefined
    }

    this.pptx = {
      title: undefined,
      url: undefined
    }
  }

  ngOnInit(): void {
  }

  OnVideoSelected(event){
    this.LoadingVideo = true;
    const file: any = Array.from(event.target.files)[0];
    if(file){
      this.video.title = file.name
      this.SmilService.upload_video(file).subscribe((event) => {

        if (event.type === HttpEventType.UploadProgress) {
          this.PercentVideo = Math.round(100 * event.loaded / event.total);
        } 
        else if (event instanceof HttpResponse) {
          this.video.url = event.body["data"].path;
          this.LoadingVideo = false;
        }
      },
      err => {
        console.log(err);
        this.LoadingVideo = false;
      });
    }
  }

  OnPPTXSelected(event){
    this.LoadingPPTX = true;
    const file: any = Array.from(event.target.files)[0];
    if(file){
      this.pptx.title = file.name
      this.SmilService.upload_pptx(file).subscribe((event: any) => {

        if (event.type === HttpEventType.UploadProgress) {
          this.PercentPPTX = Math.round(100 * event.loaded / event.total);
        } 
        else if (event instanceof HttpResponse) {
          this.pptx.url = event.body["data"].path;
          this.LoadingPPTX = false;
        }
      },
      err => {
        console.log(err);
        this.LoadingPPTX = false;
      });
    }
  }

  onDeleteVideo(){
    if(this.video.url)
      this.SmilService.delete_video(this.video.url).subscribe(res =>{
        this.video.url = null;
        this.video.title = null;
      });
  }

  onDeletePPTX(){
    if(this.pptx.url)
      this.SmilService.delete_pptx(this.pptx.url).subscribe(res =>{
        this.pptx.url = null;
        this.pptx.title = null;
      });
  }

  createJsonSmil(){
    this.LoadingCreate = true;
    console.log(this.video.url)
    console.log(this.pptx.url)
    this.SmilService.create(this.video.url, this.pptx.url).subscribe(value => {
      this.JsonValue = JSON.stringify(value,null, 2);

      this.video = {
        title: undefined,
        url: undefined
      }
  
      this.pptx = {
        title: undefined,
        url: undefined
      }

      this.LoadingCreate = false;
    })
  }
}
