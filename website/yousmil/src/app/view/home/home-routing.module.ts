import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { MyVideoComponent } from './my-video/my-video.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  { path: 'my-account', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'my-videos', component: MyVideoComponent, canActivate: [AuthGuard] },
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
