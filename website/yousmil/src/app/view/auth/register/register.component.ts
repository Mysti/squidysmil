import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterForm, RegisterFormProvider } from 'src/app/forms/register.form';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [
    RegisterFormProvider
  ]
})
export class RegisterComponent implements OnInit {

  public loading = false;
  public hide: boolean = true;
  public messageError: string = undefined;

  constructor(
    public form: RegisterForm,
    private service: UserService,
    private router: Router) { }

  ngOnInit(): void {
  }

  submitForm(): void {
    this.loading = true;
    this.service.save(this.form.getValues()).subscribe(
      (data) => {
        this.loading = false;
        this.router.navigate(['login']);
      },
      (error) => console.log(error));

  }
}
