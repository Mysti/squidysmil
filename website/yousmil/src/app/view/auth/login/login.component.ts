import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginForm, loginFormProvider } from 'src/app/forms/login.form';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [
        loginFormProvider
    ]
})

export class LoginComponent implements OnInit, OnDestroy {
    public loading = false;
    public messageError: string = undefined;
    public hide: boolean = true;

    constructor(
    public form: LoginForm,
    private service: AuthService,
    private router: Router) {}

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        console.log("destroy")
    }

    submitForm(): void {
        this.loading = true;
        this.service.login(this.form.getValues()).subscribe(
            data => {
                const token = data.headers.get('Authorization').replace('Bearer ', '');
                const user = data.body.data;

                this.service.setLogin(token, user);
                this.loading = false;

                this.router.navigate(['/home']);
            },
            err => {
                this.loading = false;
            }
        )

        this.loading = false;
    }
}
