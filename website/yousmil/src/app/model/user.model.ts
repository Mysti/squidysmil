export class User {

    idUser: number;
    lastname: string;
    firstname: string;
    email: string;
    avatar: string;
    username: string;
    password: string;
    deleted: boolean;

}