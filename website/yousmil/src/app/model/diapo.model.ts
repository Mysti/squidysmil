export class Diapo {

    id: number;

    path: string;
    title: string;
    description: string;

    click: number;
    time: number;
    numberTotal: number;

    creationDate: Date;
    deleted: boolean;
}