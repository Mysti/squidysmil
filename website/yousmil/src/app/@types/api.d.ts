declare type ApiErrorItem = {
    status?: number;
    code?: number|string;
    title: string;
    detail?: string;
}

declare type ApiSuccessMeta= {
    copyright?: string,
    authors?: string[],
}

declare type ApiSuccessLinks = {
    self?: string, // http://example.com/posts
    next?: string, // http://example.com/articles?page[offset]=2,
    last?: string, // http://example.com/articles?page[offset]=10
    total?: number
}

// Helpers: https://github.com/DefinitelyTyped/DefinitelyTyped/issues/26510
// type Diff<T extends string, U extends string> = ({ [P in T]: P } & { [P in U]: never } & { [x: string]: never })[T];
// type Omit<T, K extends keyof T> = Pick<T, Diff<keyof T, K>>;
// declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

declare type ApiSuccess = { data:any, links?: Partial<ApiSuccessLinks> }
declare type ApiError = { errors: ApiErrorItem[] }