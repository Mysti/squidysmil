import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Diapo } from '../model/diapo.model';

export class DiapoForm extends FormGroup{

    constructor(fb: FormBuilder){
        super({
            click: fb.control('', [ Validators.required ]),
            creationDate: fb.control('', [ Validators.required ]),
            deleted: fb.control('', [ Validators.required ]),
            description: fb.control('', [ Validators.required ]),
            idDiapo: fb.control('', [ Validators.required ]),
            numberTotal: fb.control('', [ Validators.required ]),
            path: fb.control('', [ Validators.required ]),
            time: fb.control('', [ Validators.required ]),
            title: fb.control('', [ Validators.required ]),
        });
    }

    public getValues(){
        let value = this.value;
        let diapo = new Diapo();

        diapo.click = value.click;
        diapo.creationDate = value.creationDate;
        diapo.deleted = value.deleted;
        diapo.description = value.description;
        diapo.id = value.idDiapo;
        diapo.numberTotal = value.numberTotal;
        diapo.path = value.path;
        diapo.time = value.time;
        diapo.title = value.title;

        return diapo;
    }

    public fillValues(diapo) {
        this.get("click").setValue(diapo.click);
        this.get("creationDate").setValue(diapo.creationDate);
        this.get("deleted").setValue(diapo.deleted);
        this.get("description").setValue(diapo.description);
        this.get("idDiapo").setValue(diapo.id);
        this.get("numberTotal").setValue(diapo.numberTotal);
        this.get("path").setValue(diapo.path);
        this.get("time").setValue(diapo.time);
        this.get("title").setValue(diapo.title);
    }

}

export let loginFormProvider = {
    provide: DiapoForm,
    useFactory: (fb: FormBuilder) => new DiapoForm(fb),
    deps: [ FormBuilder ]
};
