import { FormBuilder, FormGroup, Validators } from '@angular/forms';

export class LoginForm extends FormGroup{

    constructor(fb: FormBuilder){
        super({
            username: fb.control('', [ Validators.required ]),
            password: fb.control('', [ Validators.required ])
        });
    }

    public getValues(){
        return this.value;
    }

}

export let loginFormProvider = {
    provide: LoginForm,
    useFactory: (fb: FormBuilder) => new LoginForm(fb),
    deps: [ FormBuilder ]
};
