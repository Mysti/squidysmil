import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../model/user.model';

function MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (!control.value && !matchingControl.value) {
            return null;
        }

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            return null;
        }

        if (control.value !== matchingControl.value) {
            return { mustMatch: true };
        } else {
            return null;
        }
    }
}

export class ProfileForm extends FormGroup{

    constructor(fb: FormBuilder){
        super({
            id: fb.control('', [ ]),
            username: fb.control('', [ Validators.required ]),
            password: fb.control(''),
            confirmPassword: fb.control(''),
            avatar: fb.control(''),
            firstname: fb.control('', [ Validators.required ]),
            lastname: fb.control('', [ Validators.required ]),
            email: fb.control('', [ Validators.required ])
        },
        {
            validators: MustMatch("password","confirmPassword")
        }
        );
    }

    public getValues(): User {
        let value = this.value;
        let user: User = new User();

        if(value.id)
            user.idUser = value.id;
        user.username = value.username;

        if(value.password){
            user.password = value.password;
        }
        
        user.lastname = value.lastname;
        user.firstname = value.firstname;
        user.email = value.email;
        user.avatar = value.avatar;
        user.deleted = false;

        return user;
    }

    public fillValues(user: User) {
        this.get("id").setValue(user.idUser);
        this.get("username").setValue(user.username);
        this.get("password").setValue(user.password);
        this.get("firstname").setValue(user.firstname);
        this.get("lastname").setValue(user.lastname);
        this.get("email").setValue(user.email);
    }

    public setAvatar(path) {
        this.controls['avatar'].setValue(path);
    }
}

export let ProfileFormProvider = {
    provide: ProfileForm,
    useFactory: (fb: FormBuilder) => new ProfileForm(fb),
    deps: [ FormBuilder ]
};
