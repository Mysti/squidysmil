import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class TokenService {
    private token: string;

    constructor() {
        this.token = localStorage.getItem('token');
    }

    public setToken(token: string): void{
        this.token = token;
        localStorage.setItem('token', token);
    }

    public getToken(): string {
        return this.token;
    }

    public removeToken(): void {
        this.token = undefined;
        localStorage.removeItem('token');
    }
}

