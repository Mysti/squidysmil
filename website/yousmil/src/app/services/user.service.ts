import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { ServiceHttp } from "./serviceHttp.service";

@Injectable({
    providedIn: 'root'
})

export class UserService extends ServiceHttp  {

    constructor(private http: HttpClient){
        super(http);
    }

    public upload_avatar(file){
        let formdata: FormData = new FormData();
        formdata.append("avatar", file);

        return this.http.post(`${environment.api}/file/avatar`, formdata).pipe(
            map((response: ApiSuccess) => {
              return response.data;
            })
          );
    }

    public delete_avatar(path){
        
        return this.http.delete(`${environment.api}/file/avatar`, {params: {path}}).pipe(
            map((response: ApiSuccess) => {
              return response.data;
            })
          );
    }

}

export let factory = (httpClient: HttpClient) => {
    const service = new UserService(httpClient);
    service.setApiUrl(`${environment.api}/user`);
    return service;
};

export let UserServiceProvider = {
    provide: UserService,
    useFactory: factory,
    deps: [HttpClient, Router]
};
