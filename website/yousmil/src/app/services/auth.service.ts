import { HttpClient, HttpErrorResponse, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { User } from "../model/user.model";
import { ServiceHttp } from "./serviceHttp.service";
import { TokenService } from "./token.service";

@Injectable()
export class AuthService extends ServiceHttp  {

    private _user: User;
    private _isAuth: boolean;

    public isAuthSubject: Subject<boolean>;
    public userSubject: Subject<User>;

    constructor(
        private http: HttpClient,
        private router: Router,
        private tokenService: TokenService,
    ){
        super(http);
        this.isAuthSubject = new Subject<boolean>();
        this.userSubject = new Subject<User>();
        this.tokenService.setToken(localStorage.getItem('token'));
        
        if(this.tokenService.getToken()){
            this.isAuth().then(val =>{
                console.log(val)
                console.log(this.tokenService.getToken());
                if(val) 
                    this.router.navigate(['home']);
            });
        }
        else {
            this._isAuth = false;
            this._user = null;
        }
    }

    public login(body: any): any{
        return this.http.post(
            `${environment.api}/user/login`, 
            body, 
            {observe: 'response'}
        );
    }

    public logout(): any{
        this.tokenService.removeToken();
        this._user = null;
        this._isAuth =false;
        this.emitUser();
        this.emitIsAuth();

        window.location.assign("http://localhost:4200/login");
    }

    public setLogin(token: string, user: any): void{
        this.tokenService.setToken(token);
        this._user = user;
        this._isAuth = true;
        this.emitUser();
        this.emitIsAuth();
    }

    public emitIsAuth(): void {
        this.isAuthSubject.next(this._isAuth);
    }

    public emitUser(): void {
        if(this._user)
            this.userSubject.next(JSON.parse(JSON.stringify(this._user)));
        else
            this.userSubject.next(this._user);
    }

    public async isAuth(): Promise<boolean> {
        if(this.tokenService.getToken() !== null || this.tokenService.getToken() !== undefined || this.tokenService.getToken() !== ''){
            let response: any = await this.http.get(`${environment.api}/user/me`, {observe: 'response'}).toPromise()
            .then((data) => {
                return data;
            })
            .catch((error) => {
                return error;
            });

            if(response instanceof HttpErrorResponse || response instanceof Error){
                this._isAuth = false;
                this.emitIsAuth();
                this.emitUser();
                return false;
            }
            else{
                this._user = Object.assign(new User(), response.body.data);
                this._isAuth = true;
                this.emitIsAuth();
                this.emitUser();
                return true;
            }
        } else {
            this._isAuth = false;
            this.emitIsAuth();
            this.emitUser();
            return false;
        }
        
    }

    public updateUser(): void {
        let response: any = this.http.get(`${environment.api}/user/me`, {observe: 'response'}).subscribe(
            (res) => {
                this._user = Object.assign(new User(), response.body.data);
                this.emitUser();
            },
            (error) => {
                this.logout();
            });
    }

    public setUser(user: User): void {
        this._user = user;
        this.emitUser();
    } 
}

export let factory = (httpClient: HttpClient, router: Router, token: TokenService) => {
    const service = new AuthService(httpClient, router, token);
    service.setApiUrl(`${environment.api}/user`);
    return service;
};

export let AuthServiceProvider = {
    provide: AuthService,
    useFactory: factory,
    deps: [HttpClient, Router, TokenService]
};
