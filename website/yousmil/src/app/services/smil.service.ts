import { HttpClient, HttpEvent, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { ServiceHttp } from "./serviceHttp.service";

@Injectable({
    providedIn: 'root'
})

export class SmileService extends ServiceHttp  {

    constructor(private http: HttpClient){
        super(http);
    }

    public upload_video(file){
        let formdata: FormData = new FormData();
        formdata.append("file", file);

        return this.http.post(`${environment.apiSmil}/file/video`, formdata, {observe: "events", reportProgress: true});
    }

    public upload_pptx(file){
        let formdata: FormData = new FormData();
        formdata.append("file", file);

        return this.http.post(`${environment.apiSmil}/file/pptx`, formdata, {observe: "events", reportProgress: true});
    }

    public delete_video(path){
        
        return this.http.delete(`${environment.apiSmil}/file/video`, {params: {path}}).pipe(
            map((response: ApiSuccess) => {
              return response.data;
            })
          );
    }

    public delete_pptx(path){
        
        return this.http.delete(`${environment.apiSmil}/file/pptx`, {params: {path}}).pipe(
            map((response: ApiSuccess) => {
              return response.data;
            })
          );
    }

    public create(video, pptx){
        return this.http.get(`${environment.apiSmil}/smil/create`, {
            params: {
                video,
                pptx
            }
        }).pipe(
            map((response: ApiSuccess) => {
                console.log(response);
              return response.data;
            }));
    }
}

export let factory = (httpClient: HttpClient) => {
    const service = new SmileService(httpClient);
    service.setApiUrl(`${environment.apiSmil}/smil`);
    return service;
};

export let UserServiceProvider = {
    provide: SmileService,
    useFactory: factory,
    deps: [HttpClient, Router]
};
