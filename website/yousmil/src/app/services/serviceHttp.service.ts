import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

export class ServiceHttp {

  protected readonly headers = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  protected apiUrl: string;

  constructor(protected httpClient: HttpClient) {
  }

  setApiUrl(apiUrl: string){
    this.apiUrl = apiUrl;
    return this;
  }

  public getApiUrl(): string {
    return this.apiUrl;
  }

  public get(id) {
    let headers = this.headers;

    return this.httpClient.get(
      `${this.apiUrl}/${id}`,
      {headers}
    ).pipe(
      map((response: ApiSuccess) => {
        return response.data;
      })
    );
  }

  public save(data){
    let body = data;
    let headers = this.headers;

    return this.httpClient.put(
      `${this.apiUrl}`,
      body,
      {headers}
    ).pipe(
      map((response: ApiSuccess) => {
        return response.data;
      })
    );
  }

  public delete(id){
    let headers = this.headers;

    return this.httpClient.delete(
      `${this.apiUrl}/${id}`,
      {headers}
    ).pipe(
      map((response: ApiSuccess) => {
        return response.data;
      })
    );
  }

  public getList(){
    let headers = this.headers;

    return this.httpClient.get(
      `${this.apiUrl}/list`,
      {headers}
    ).pipe(
      map((response: ApiSuccess) => {
        return response.data;
      })
    );
  }

}
