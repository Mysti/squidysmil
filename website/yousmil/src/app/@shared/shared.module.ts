import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthServiceProvider } from '../services/auth.service';
import { tokenInterceptorProvider } from '../interceptor/token.interceptor';
import { flashInterceptorProvider } from '../interceptor/flash.interceptor';
import { TokenService } from '../services/token.service';
import { AuthGuard } from '../guards/auth.guard';
import { UserServiceProvider } from '../services/user.service';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [
  ],
  exports: [
    MaterialModule,
    HttpClientModule,
  ]
})
export class SharedModule { }
