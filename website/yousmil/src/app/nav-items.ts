export class NavItem {
    name: string;
    path: string;
    icon: string;
}

export class NavItemGroup {
    title: string;
    items: NavItem[];

    constructor(title: string, items: NavItem[]) {
        this.title = title;
        if(items)
            this.items = items;
        else
            this.items = [];
    }

    addItem(item: NavItem) {
        this.items.push(item);
    }

    removeItem(item: NavItem) {
        this.items = this.items.filter(el => el.name !== item.name);
    }
}

let listVideo = [
    {
        name: "Home",
        path: "/home",
        icon: "home"
    },
    {
        name: "Subscriptions",
        path: "/home",
        icon: "book"
    },
    {
        name: "My videos",
        path: "/home/my-videos",
        icon: "video_settings"
    }
]
let video = new NavItemGroup("Video", listVideo)
let listParameters = [
    {
        name: "My profile",
        path: "/home/my-account",
        icon: "person"
    },
    {
        name: "Parameters",
        path: "/home",
        icon: "edit"
    }
]
let parameters = new NavItemGroup("Application", listParameters)

export const items : NavItemGroup[] = [
    video,
    parameters
]