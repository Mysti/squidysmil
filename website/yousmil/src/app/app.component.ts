import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from './model/user.model';
import { items } from './nav-items';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  public title = 'yousmil';
  public mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  public menuItemsGroups = items;
  private isAuthSubscription: Subscription;
  private userSubscription: Subscription;
  public isAuth: boolean;
  public user: User;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public authService: AuthService,
    private router: Router) {

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(): void {
    this.isAuthSubscription = this.authService.isAuthSubject.subscribe(val => {
      this.isAuth = val
    });
    this.userSubscription = this.authService.userSubject.subscribe(val => {
      this.user = val
    });

    this.authService.emitIsAuth();
    this.authService.emitUser();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.isAuthSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }

  logout(): void {
    this.authService.logout();
  }
  
  changePage(path){
    this.router.navigate([path])
  }

}
