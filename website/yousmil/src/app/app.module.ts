import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { tokenInterceptorProvider } from './interceptor/token.interceptor';
import { flashInterceptorProvider } from './interceptor/flash.interceptor';
import { AuthServiceProvider } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { UserServiceProvider } from './services/user.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './@shared/shared.module';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AvatarModule } from 'ngx-avatar';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    FlashMessagesModule.forRoot(),
    AvatarModule
  ],
  providers: [
    UserServiceProvider,
    AuthServiceProvider,
    tokenInterceptorProvider,
    flashInterceptorProvider,

    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
