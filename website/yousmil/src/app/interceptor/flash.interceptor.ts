import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'

@Injectable()
export class FlashInterceptor implements HttpInterceptor{
    //private flashService: FlashMessagesService
    constructor(private flashService: FlashMessagesService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {
                let errors: any[] = error.error.errors;
                if(errors)
                    errors.forEach(ApiError => {
                        this.flashService.show(`<p>${ApiError.title}</p> <p>Status:${error.status}</p> <p>${ApiError.detail}</p>`, { cssClass: 'alert-danger'})
                    })
                return throwError(error);
            })
        ); 
    }
}

export const flashInterceptorProvider = {
        provide: HTTP_INTERCEPTORS,
        useClass: FlashInterceptor,
        multi: true,
    };

