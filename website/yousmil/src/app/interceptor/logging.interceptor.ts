import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class LoggingInterceptor implements HttpInterceptor{

    constructor() {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        //faire le logging

        return next.handle(req);
    }
}

export const logginInterceptorProvider = {
        provide: HTTP_INTERCEPTORS,
        useClass: LoggingInterceptor,
        multi: true,
    };

