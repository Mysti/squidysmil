import { Request, Response } from "express";
import { v4 as uuidv4 } from 'uuid';
import * as multer from 'multer';
import * as fs from 'fs';
import * as urljoin from 'url-join';
import * as path from "path";
import { ApiError, ApiSuccess } from "../model/api.model";

export class FileMiddleware{

    public static DIR_TEMP: string = "public/temps";
    public static DIR_PICTURE: string = "public/pictures";
    public static DIR_VIDEO: string = "public/videos";
    public static DIR_PPTX: string = "public/pptx";
    
    public static upload_image(temp: boolean){
        let dir: string = temp ? this.DIR_TEMP : this.DIR_PICTURE;

        const storage_images = multer.diskStorage({
            destination: (req: any, file: any, cb: (arg0: any, arg1: any) => void) => {
                cb(null, dir);
            },
            filename: (req, file, cb) => {
                const fileName = generateFileName(file.originalname);
                cb(null, fileName)
            }
        });

        // Multer Mime Type Validation
        return multer({
            storage: storage_images,
            limits: {
                fileSize: 1024 * 1024 * 50
            },
            fileFilter: (req, file, cb) => {
                if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
                    cb(null, true);
                } else {
                    cb(null, false);
                    return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
                }
            }
        }).single('file');
    }

    public static upload_video(temp: boolean){
        let dir: string = temp ? this.DIR_TEMP : this.DIR_VIDEO;

        const storage_images = multer.diskStorage({
            destination: (req: any, file: any, cb: (arg0: any, arg1: any) => void) => {
                cb(null, dir);
            },
            filename: (req, file, cb) => {
                const fileName = generateFileName(file.originalname);
                cb(null, fileName)
            }
        });

        // Multer Mime Type Validation
        return multer({
            storage: storage_images,
            limits: {
                fileSize: 1024 * 1024 * 50
            },
            fileFilter: (req, file, cb) => {
                if (file.mimetype == "video/wav" || file.mimetype == "video/mp3" || file.mimetype == "video/mp4"
                || file.mimetype == "video/mpg" || file.mimetype == "video/wmv" || file.mimetype == "video/avi"
                || file.mimetype == "video/quicktime") {
                    cb(null, true);
                } else {
                    cb(null, false);
                    return cb(new Error('Only videos .wav, .mp3, .mp4, .mpg, .wmv, .mov and .avi format allowed!'));
                }
            }
        }).single('file');
    }

    public static upload_pptx(temp: boolean){
        let dir: string = temp ? this.DIR_TEMP : this.DIR_PPTX;

        const storage_images = multer.diskStorage({
            destination: (req: any, file: any, cb: (arg0: any, arg1: any) => void) => {
                cb(null, dir);
            },
            filename: (req, file, cb) => {
                const fileName = generateFileName(file.originalname);
                cb(null, fileName)
            }
        });

        // Multer Mime Type Validation
        return multer({
            storage: storage_images,
            limits: {
                fileSize: 1024 * 1024 * 50
            },
            fileFilter: (req, file, cb) => {
                if (file.mimetype == "application/vnd.openxmlformats-officedocument.presentationml.presentation") {
                    cb(null, true);
                } else {
                    cb(null, false);
                    return cb(new Error('Only .pptx format allowed!'));
                }
            }
        }).single('file');
    }
    
    public static sendFileLink(req: Request, res: Response, next) {
        const urlPrefix = `${req.protocol}://${req.get('host')}`;
        const elementUrl = urljoin(urlPrefix, req.file.path).split("\\").join("/");
        res.status(200).json(new ApiSuccess({path: elementUrl}));
    }

    public static deleteFile(req: Request, res: Response, next) {
        try{
            let url: any = req.query.path;
            let pathFile = url.split(`${req.protocol}://${req.get('host')}`)[1]
            pathFile = path.join(__dirname,"../",pathFile) 
            

            fs.unlinkSync(pathFile);
            res.status(200).json(new ApiSuccess());
        }
        catch(e){
            return res.status(500).json(new ApiError({
                code: e.code,
                title: "Cannot delete the file.",
                detail: "File doesn't exist or not permission to delete"
            }));
        }

    }
}

export function generateFileName(oldFilename: string) {
    let fileExtension = (/[.]/.exec(oldFilename)) ? /[^.]+$/.exec(oldFilename)[0] : undefined;
    let filename: string = `${uuidv4().replace(/-/g, "")}.${fileExtension}`;
    return filename;
}

export function isTempFile(fileURL: string){
    let include = path.join(FileMiddleware.DIR_TEMP).split("\\").join("/");
    return fileURL.includes(include);
}

export function tempToPicture(fileURL: string){
    let temp = path.join(FileMiddleware.DIR_TEMP).split("\\").join("/");
    let image = path.join(FileMiddleware.DIR_PICTURE).split("\\").join("/");
    let newURL = fileURL.replace(temp, image);
    let pathname = new URL(newURL).pathname.replace("/", "");
    let tempPathname = new URL(fileURL).pathname.replace("/", "");

    fs.copyFileSync(
        path.join(__dirname, "..", tempPathname),
        path.join(__dirname, "..", pathname)
    );
    fs.unlinkSync(path.join(__dirname, "..", tempPathname));
}

export function tempToVideo(fileURL: string){
    let temp = path.join(FileMiddleware.DIR_TEMP).split("\\").join("/");
    let image = path.join(FileMiddleware.DIR_VIDEO).split("\\").join("/");
    let newURL = fileURL.replace(temp, image);
    let pathname = new URL(newURL).pathname.replace("/", "");
    let tempPathname = new URL(fileURL).pathname.replace("/", "");

    fs.copyFileSync(
        path.join(__dirname, "..", tempPathname),
        path.join(__dirname, "..", pathname)
    );
    fs.unlinkSync(path.join(__dirname, "..", tempPathname));
}

export function tempToPPTX(fileURL: string){
    let temp = path.join(FileMiddleware.DIR_TEMP).split("\\").join("/");
    let image = path.join(FileMiddleware.DIR_PPTX).split("\\").join("/");
    let newURL = fileURL.replace(temp, image);
    let pathname = new URL(newURL).pathname.replace("/", "");
    let tempPathname = new URL(fileURL).pathname.replace("/", "");

    fs.copyFileSync(
        path.join(__dirname, "..", tempPathname),
        path.join(__dirname, "..", pathname)
    );
    fs.unlinkSync(path.join(__dirname, "..", tempPathname));
}
