import { Request, Response } from "express";
import * as fs from "fs";
import * as path from "path";
import { ApiError, ApiSuccess } from "../model/api.model";
const {spawn} = require('child_process');

export class SmilMiddleware{


    public static create(req: Request, res: Response, next) {

        try {
            let video: any = req.query.video;
            let videoPath = video.split(`${req.protocol}://${req.get('host')}`)[1];
            videoPath = path.join(__dirname,"../",videoPath);
            
            let pptx: any = req.query.pptx;
            let pptxPath = pptx.split(`${req.protocol}://${req.get('host')}`)[1];
            pptxPath = path.join(__dirname,"../",pptxPath);
    
            let responseData;
            let script = path.join(__dirname, "../script/toJson.py")
            const python = spawn('python', [script, videoPath, pptxPath]);
    
            python.stdout.on('data', function (data) {
                responseData = JSON.parse(data.toString());
            });
    
            python.on('close', (code) => {

                try{
                    fs.unlinkSync(videoPath);
                    fs.unlinkSync(pptxPath);
                } catch(e) {
                    return res.status(404).json(new ApiError({
                        title: "Not Found",
                        detail: "files not found"
                    }))
                }

                if(!responseData){
                    return res.status(500).json(new ApiError({
                        title: "Unknow Error",
                        detail: "No result"
                    }))
                }
                
                // send data to browser
                return res.status(200).json(new ApiSuccess(responseData));

            });
        }
        catch(e){
            return res.status(500).json(new ApiError({
                title: "Unknow Error"
            }))
        }
    }
}