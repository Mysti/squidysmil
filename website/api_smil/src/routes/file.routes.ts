import { Application } from "express-serve-static-core";
import { FileMiddleware } from "../middleware/file.middleware";

module.exports = function(app: Application){
    
    app.route("/file/video")
    .post(
        FileMiddleware.upload_video(true),
        FileMiddleware.sendFileLink,
    )
    .delete(
        FileMiddleware.deleteFile
    );

    app.route("/file/picture")
    .post(
        FileMiddleware.upload_image(true),
        FileMiddleware.sendFileLink,
    )
    .delete(
        FileMiddleware.deleteFile
    );

    app.route("/file/pptx")
    .post(
        FileMiddleware.upload_pptx(true),
        FileMiddleware.sendFileLink,
    )
    .delete(
        FileMiddleware.deleteFile
    );


}