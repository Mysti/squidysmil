import { Application } from "express-serve-static-core";
import { SmilMiddleware } from "../middleware/smil.middleware";

module.exports = function(app: Application){
    
    app.route("/smil/create")
    .get(
        SmilMiddleware.create
    );


}