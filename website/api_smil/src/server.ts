import * as express from "express";
import * as bodyParser from "body-parser";
import * as glob from "glob";
import * as path from "path";
import * as cors from "cors";
import * as fs from "fs";
import { Connection, createConnection } from "typeorm";
import { FileMiddleware } from "./middleware/file.middleware";

export class Server{

    private host: string;
    private port: number;
    private app: any;

    constructor(host: string, port: number, dataBaseConfig: any){
        this.host = host;
        this.port = port;
        this.app = express();

        this.app.use(bodyParser.json({
            limit: '50mb',
        }));

        this.app.use(cors())
    
        let dirtemp = path.join(__dirname, FileMiddleware.DIR_TEMP)
        if (!fs.existsSync(dirtemp)){
            fs.mkdirSync(dirtemp, {recursive: true});
        }

        let dirpic = path.join(__dirname, FileMiddleware.DIR_PICTURE)
        if (!fs.existsSync(dirpic)){
            fs.mkdirSync(dirpic, {recursive: true});
        }

        let dirpptx = path.join(__dirname, FileMiddleware.DIR_PPTX)
        if (!fs.existsSync(dirpptx)){
            fs.mkdirSync(dirpptx, {recursive: true});
        }

        let dirvideo = path.join(__dirname, FileMiddleware.DIR_VIDEO)
        if (!fs.existsSync(dirvideo)){
            fs.mkdirSync(dirvideo, {recursive: true});
        }

        // Make Folder Publicly Available
        this.app.use('/public/pictures', express.static('public/pictures'));
        this.app.use('/public/temps', express.static('public/temps'));
        this.app.use('/public/videos', express.static('public/videos'));
        this.app.use('/public/pptx', express.static('public/pptx'));
        
        this.setDataBase(dataBaseConfig);
        this.setRoute();
    }

    private setDataBase(dataBaseConfig: any){
        createConnection(dataBaseConfig).then((connection: Connection) => {
            this.app.use(bodyParser.json({
                limit: '50mb'
            }));
        });
    }

    private setRoute(){
        glob.sync(path.join(__dirname,'./routes/*.routes.*s')).forEach(route => {
            console.log(`loading ${route}...`)
            require(path.resolve(route))(this.app);
        });
        console.log(`routes loaded`)
    }

    start(){
        this.app.listen(this.port, () => {
            console.log(`Server started on port ${this.host}:${this.port}.`)
        });

    }

}
