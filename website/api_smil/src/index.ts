/**
 * @author DE BRUYNE Alexis - LECELLIER Laetitia 
 * Last Update: 25/01/2021
 */

const Config = require("./config/config");
import { Server } from "./server";

const host = Config.get("host");
const port = Config.get("port");

const server = new Server(host,port, Config.get("db"));
server.start();